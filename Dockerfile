FROM centos
MAINTAINER debugging "debugging@163.com"
RUN yum install -y wget
RUN yum install -y tar
RUN yum install -y openssh openssh-server openssh-clients
RUN mkdir /var/run/sshd
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
RUN ssh-keygen -t dsa -f /etc/ssh/ssh_host_dsa_key
RUN /bin/echo 'root:123456' |chpasswd
RUN useradd admin
RUN /bin/echo 'admin:123456' |chpasswd
RUN /bin/sed -i 's/.*session.*required.*pam_loginuid.so.*/session optional pam_loginuid.so/g' /etc/pam.d/sshd
RUN /bin/echo -e "LANG=\"en_US.UTF-8\"" > /etc/default/local
RUN yum install java-1.8.0-openjdk -y
RUN mkdir /home/tomcat /home/docker /home/docker/docker_plugins
WORKDIR /home/docker/docker_plugins 
RUN wget http://git.oschina.net/itkang/docker_plugins/raw/master/apache-tomcat-8.0.15.tar
RUN tar -xf apache-tomcat-8.0.15.tar
RUN mv apache-tomcat-8.0.15 /home/tomcat
EXPOSE 22
EXPOSE 8080
ENTRYPOINT /home/tomcat/apache-tomcat-8.0.15/bin/startup.sh && /usr/sbin/sshd -D
