var zr; // 全局可用zrender对象
var editor = document.getElementById("code")
function refresh(isBtnRefresh) {

    (new Function(editor.value))();
}
var developMode = 0;
if (developMode) {
    // for develop
    require.config({
        packages: [
            {
                name: 'zrender',
                location: 'build',
                main: 'zrender1'
            }
        ]
    });
}
else {
    // for echarts online home page
    var fileLocation = 'build/zrender1';
    require.config({
        paths: {
            zrender: fileLocation,
            'zrender/shape/Rose': fileLocation,
            'zrender/shape/Trochoid': fileLocation,
            'zrender/shape/Circle': fileLocation,
            'zrender/shape/Sector': fileLocation,
            'zrender/shape/Ring': fileLocation,
            'zrender/shape/Ellipse': fileLocation,
            'zrender/shape/Rectangle': fileLocation,
            'zrender/shape/Text': fileLocation,
            'zrender/shape/Heart': fileLocation,
            'zrender/shape/Droplet': fileLocation,
            'zrender/shape/Line': fileLocation,
            'zrender/shape/Image': fileLocation,
            'zrender/shape/Star': fileLocation,
            'zrender/shape/Isogon': fileLocation,
            'zrender/shape/BezierCurve': fileLocation,
            'zrender/shape/Polyline': fileLocation,
            'zrender/shape/Path': fileLocation,
            'zrender/shape/Polygon': fileLocation
        }
    });
}


require(
    [
        'zrender',
        'zrender/shape/Circle'
    ],
    function (zrender,CircleShape) {
        zr = zrender.init(document.getElementById('main'));
// 圆形
        zr.addShape(new CircleShape({
            style: {
                x: 100,
                y: 100,
                r: 50,
                brushType: 'both',
                color: 'rgba(220, 20, 60, 0.8)',          // rgba supported
                strokeColor: 'rgba(220, 20, 60, 0.8)',  // getColor from default palette
                lineWidth: 5,
                text: 'circle',
                textPosition: 'inside'
            },
            //hoverable: true,   // default true
            //draggable: true,   // default false
            //clickable: true,   // default false

            // 可自带任何有效自定义属性
            _name: 'Hello~',
            onclick: function (params) {
                alert(params.target._name);
            },

            // 响应事件并动态修改图形元素
            onmousewheel: function (params) {
                var eventTool = require('zrender/tool/event');
                var delta = eventTool.getDelta(params.event);
                var r = params.target.style.r;
                r += (delta > 0 ? 1 : -1) * 10;
                if (r < 10) {
                    r = 10;
                }
                ;
                zr.modShape(params.target.id, {style: {r: r}})
                zr.refresh();
                eventTool.stop(params.event);
            }
            /* 封装支持事件，见shape/base, config.EVENT
             onmousemove : function(e){console.log('onmousemove',e)},
             onmouseover : function(e){console.log('onmouseover',e)},
             onmouseout  : function(e){console.log('onmouseout',e)},
             onmousedown : function(e){console.log('onmousedown',e)},
             onmouseup   : function(e){console.log('onmouseup',e)},
             ondragstart : function(e){console.log('ondragstart',e)},
             ondragend   : function(e){console.log('ondragend',e)},
             ondragenter : function(e){console.log('ondragenter',e)},
             ondragover  : function(e){console.log('ondragover',e)},
             ondragleave : function(e){console.log('ondragleave',e)},
             ondrop      : function(e){console.log('ondrop',e)}
             */
        }));

        zr.render();

        /* 除了在shape上绑定事件，可以挂接全局事件
         zr.on('click',      function(e){console.log('onclick',e,'global')});
         zr.on('mousewheel', function(e){console.log('onmousewheel',e,'global')});
         zr.on('mousemove',  function(e){console.log('onmousemove',e,'global')});
         zr.on('mouseover',  function(e){console.log('onmouseover',e,'global')});
         zr.on('mouseout',   function(e){console.log('onmouseout',e,'global')});
         zr.on('mousedown',  function(e){console.log('onmousedown',e,'global')});
         zr.on('mouseup',    function(e){console.log('onmouseup',e,'global')});
         zr.on('dragstart',  function(e){console.log('ondragstart',e,'global')});
         zr.on('dragend',    function(e){console.log('ondragend',e,'global')});
         zr.on('dragenter',  function(e){console.log('ondragenter',e,'global')});
         zr.on('dragover',   function(e){console.log('ondragover',e,'global')});
         zr.on('dragleave',  function(e){console.log('ondragleave',e,'global')});
         zr.on('drop',       function(e){console.log('ondrop',e,'global')});
         */
    });
